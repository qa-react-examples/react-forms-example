import React from 'react';

export default class Forms extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            myInput: 'default'
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(data) {
        this.setState({
            myInput: data.target.value
        });
    }

    render() {
        return (
            <div>
                <h1>Forms {this.state.myInput}</h1>
                <input type="text" value={this.state.myInput} onChange={this.handleChange} />
            </div>
        );
    }
}